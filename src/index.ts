import crypto from 'crypto';
import axios from 'axios';
import { fetch } from 'cross-fetch';
import oauth1a from 'oauth-1.0a';

const ACCOUNT_ID = '5546124';
const PROD_CONSUMER_KEY='f47c4aabcba45c753602215176121d0f836889881b5cfd0961723d314f193793';
const PROD_CONSUMER_SECRET='049b123a428246c620ea3b74ce94240ab7fb7019360c01be79ec1fcfc329e6cd';

const PROD_TOKEN_ID='317210b6481792899af615709a3868d95612c3d88ae9b5b3103681bdd7584858';
const PROD_TOKEN_SECRET='6d3abd0526b963a1a87cbffcf3128f999b48e94c747f67a494fb28981c78dd56';

const genOAuthHeader = (req: oauth1a.RequestOptions) => {
    const oauth = new oauth1a({
        consumer: {
            key: PROD_CONSUMER_KEY,
            secret: PROD_CONSUMER_SECRET,
        },
        signature_method: 'HMAC-SHA256',
        hash_function: (baseString, key) => {
            return crypto.createHmac('sha256', key)
                .update(baseString)
                .digest('base64');
        },
        realm: ACCOUNT_ID,
    });

    const authorization = oauth.authorize(req, {
        key: PROD_TOKEN_ID,
        secret: PROD_TOKEN_SECRET,
    });

    return oauth.toHeader(authorization);
}

const main = async () => {
    const req: oauth1a.RequestOptions = {
        url: 'https://5546124.suitetalk.api.netsuite.com/services/rest/record/v1/check',
        method: 'GET',
    };

    const headers = genOAuthHeader(req);
    console.log(headers);
    try {
        const res = await fetch(req.url, {
            method: req.method,
            headers: headers as unknown as Record<string, string>,
        });
        console.log(res); 
    
        const json = await res.json();
    
        console.log(json);
    } catch (e) {
        console.error(e);
        console.log()
    }

}

main();